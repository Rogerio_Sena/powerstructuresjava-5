import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;


/**
 * 
 * Generic Sparse Matrix
 */
public class SparseMatrixFloat{
	class Cell{
		Cell right, bottom;
		int line, column;
		float value;
		
		public Cell() {}
		public Cell(int line, int column, float value){
			this.line = line;
			this.column = column;
			this.value = value;
		}
		public Cell(Cell right, Cell bottom, int line, int column, float value){
			this.right = right!=null ? right : this;
			this.bottom = bottom!=null ? bottom : this;
			this.line = line;
			this.column = column;
			this.value = value;
		}
	}
	
	private int m=0, n=0, size=0;
	private Cell first, aux;
	
	public SparseMatrixFloat() {
		this.first = new Cell(-1, -1, 0);
		this.first.bottom = this.first;
		this.first.right = this.first;
	}
	public SparseMatrixFloat(int m, int n){
		this.first = new Cell(-1, -1, 0);
		this.first.bottom = this.first;
		this.first.right = this.first;
		
		initialize(m,n);
	}
	
	public int getM() {
		return m;
	}
	public int getN() {
		return n;
	}
	public Cell getFirst(){
		return this.first;
	}
	public int getSize(){
		return this.size;
	}
	
	private void setM(int m) {
		this.m = m;
	}
	private void setN(int n) {
		this.n = n;
	}
	
	/**
	 * Creates the base to insert elements
	 * 
	 * @param m - number of lines
	 * @param n - number of columns
	 * 
	 * @author Marcos Paulo
	 */
 	public void initialize(int m, int n){
		setM(m);
		setN(n);
		
		getFirst().bottom = null;
		getFirst().right = null;
		
		Cell c;
		int cont;
		
		aux = getFirst();
		for(cont=1; cont<=getM(); cont++){
			c = new Cell(null, getFirst(), cont, -1, 0);
			aux = aux.bottom = c;
		}
		
		aux = getFirst();
		for(cont=1; cont<=getN(); cont++){
			c = new Cell(getFirst(), null, -1, cont, 0);
			aux = aux.right = c;
		}
	}

 	/**
 	 * Returns the left element of a position(i,j) 
 	 * 
 	 * @param i - line 
 	 * @param j - column
 	 * 
 	 * @return Cell
 	 * 
 	 * @author Marcos Paulo
 	 */
 	private Cell leftElement(int i, int j){
		aux = getFirst();
		while(aux.bottom.line != -1){
			aux = aux.bottom;
			if(aux.line==i){
				while(aux.right.column != -1){
					if(aux.right.column >= j)
						break;
					aux = aux.right;
				}
				break;
			}
		}
		return aux;
 	}
 	/**
 	 * Returns the top element of a position(i,j) 
 	 * 
 	 * @param i - line 
 	 * @param j - column
 	 * 
 	 * @return Cell
 	 * 
 	 * @author Marcos Paulo
 	 */
 	private Cell topElement(int i, int j){
		aux = getFirst();
		while(aux.right.column != -1){
			aux = aux.right;
			if(aux.column==j){
				while(aux.bottom.line != -1){
					if(aux.bottom.line >= i)
						break;
					aux = aux.bottom;
				}
				break;
			}
		}
		return aux;
 	}
 	
 	/**
 	 * Returns the element's value on a position(i,j)
 	 * 
 	 * @param i - line 
 	 * @param j - column
 	 * 
 	 * @return Type generic
 	 * 
 	 * @author Marcos Paulo
 	 */
 	public float get(int i, int j){
		aux = leftElement(i, j).right;
		if(aux.line == i && aux.column == j)
			return aux.value;
		return 0;
 	}

 	/**
 	 * Insert an element 
 	 * 
 	 * @param i - line
 	 * @param j - column
 	 * @param value - element's value
 	 * 
 	 * @throws Exception when the insertion position are invalid
 	 * 
 	 * @author Marcos Paulo
 	 */
	public void insert(int i, int j, float value) throws Exception{
		if(value != 0){
			if(i > 0 && j > 0 && i<=getM() && j<=getN()){
				Cell c = new Cell(i, j, value);
				
				aux = leftElement(i, j);
				c.right = aux.right;
				aux.right = c;
				
				aux = topElement(i, j);
				c.bottom = aux.bottom;
				aux.bottom = c;
				
				this.size++;
			}else
				throw new Exception("Error: invalid position!");
		}
	}
	
	/**
 	 * Remove an element 
 	 * 
 	 * @param i - line
 	 * @param j - column
 	 * 
 	 * @return float removed element's value 
 	 * 
 	 * @author Marcos Paulo
 	 */
	public float remove(int i, int j){
		aux = leftElement(i, j);
		if(aux.right.column==j){
			float f = aux.right.value;
			aux.right = aux.right.right;
			
			aux = topElement(i, j);
			aux.bottom = aux.bottom.bottom;
			
			return f;
		}else
			return 0;
	}
	
	/**
 	 * Print the matrix cells
 	 * 
 	 * @author Marcos Paulo
 	 */
	public void printMatrix(){
		System.out.println("Matrix:");
		aux = getFirst();
		Cell aux2, none= new Cell();
		int line, column;
		for(line=1; line<=getM(); line++){
			aux2 = aux = aux.bottom;
			for(column=1; column<=getN(); column++){
				if(aux2.right.column == column){
					aux2 = aux2.right;
					printCell(aux2);
				}else{
					printCell(none);
				}
			}
			System.out.println();
		}
	}
	/**
 	 * Print a cell
 	 * 
 	 * @param c - cell
 	 * 
 	 * @author Marcos Paulo
 	 */
	private void printCell(Cell c){
		if(c != null)
			System.out.print(c.line+","+c.column+" -> "+c.value+"\t");
		else
			System.out.println("NULL");
	}
	
	/**
 	 * Returns the sum of two matrixes 
 	 * 
 	 * @param Matrix A 
 	 * @param Matrix B
 	 * 
 	 * @return Matrix R
 	 * 
 	 * @author Isabelly Lima
 	 */

	public static SparseMatrixFloat sumMatrixes(SparseMatrixFloat A, SparseMatrixFloat B) throws Exception{
		if (B.getM() != A.getM() || B.getN() != A.getN())
			System.err.println("Wrong!!!!! The number of lines and columns don't match!");
		SparseMatrixFloat R = new SparseMatrixFloat(A.getM(), A.getN());
		for (int i = 1; i <= A.getM(); i++)
			for (int j = 1; j <= A.getN(); j++)				
				R.insert(i, j, B.get(i, j)+A.get(i, j));			
		return R;
	}
	
	/**
 	 * Returns the multiplication of two matrices
 	 * 
 	 * @param Matrix m
 	 * @return Matrix aux
 	 * 
 	 * @author Matheus de Souza Oliveira
 	 */

	public SparseMatrixFloat multiplyMatrices(SparseMatrixFloat m) throws Exception{
        if (this.getN() == m.getM()) {
        	SparseMatrixFloat aux = new SparseMatrixFloat(this.getN(), m.getM());
            for (int i = 0; i < this.getN(); i++) {
                for (int j = 0; j < this.getM(); j++) {
                    float value = 0;
                    for (int k = 0; k < this.getM(); k++) {
                        value += this.get(i, k) + m.get(k, j);
                    }
                    aux.insert(i, j, value);
                }
            }
            return aux;
        } else {
            throw new Exception("Error!!! Number of columns of the matrix A must equal the number of rows of the matrix B!");
        }
    }
	
	/**
 	 * Returns the difference of two matrices 
 	 * 
 	 * @param Matrix m 
 	 * 
 	 * @return Matrix aux
 	 * 
 	 * @author Bárbara Feijão
 	 */
	 public SparseMatrixFloat subMatrices(SparseMatrixFloat m) throws Exception{
     	if (this.getN() == m.getN() || this.getM() == m.getM()){
     		SparseMatrixFloat aux = new SparseMatrixFloat(this.getM(), this.getN());
			for (int i = 0; i < this.getM(); i++)
				for (int j = 0; j < this.getN(); j++)
					aux.insert(i, j, this.get(i, j)-m.get(i, j));			
			return aux;
     	}else{
        	throw new Exception("Wrong!!!!! The number of lines and columns don't match!");
      	}
	}

	 /**

 	 * This method read from a matrix to be recorded to be recorded at one point doc. 
	 * Pass the directory of the file so that the reading method can obeter values and 
	 *put in matrix.To read the file that contains the necessary information to instaciar the 
	 *matris array pass as a parameter in the method FileReader direório be where 
	 *your file on your computer. 
 	 *
	 *Example of parameter it's path to file: "C:/home/User/Workspace/Matrix.txt"
	 *
	 * Model of what is to be written in the file.
     *3,3
     *1,1,12
     *1,2,04
     *2,2,34
     *1,3,32
     *2,1,43
	 *
	 * @throws Exception 
	 * 
 	 * @author Wallison Bruno Moura
 	 */

	public void readMatrix(String pathToFile) throws Exception {
		Scanner read = new Scanner(new FileReader(pathToFile)).useDelimiter("\n|,");
		int numLine = Integer.parseInt(read.next());
		int numColum = Integer.parseInt(read.next());
		this.initialize(numLine, numColum);
		while (read.hasNext()) {
			int line = Integer.parseInt(read.next());
			int column = Integer.parseInt(read.next());
			float value = Float.parseFloat(read.next());
			this.insert(line,column,value);

		}

	}
	
}
